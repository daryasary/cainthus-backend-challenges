# Senior Backend Engineer Challenges

## Challenge 1:

Given two timestamps, in different time zones, of the format:

`Day dd Mon yyyy hh:mm:ss +xxxx`

where `+xxxx` represents the time zone. 

Your task is to print the absolute difference (in seconds) between them.

#### Input Format
- The first line contains the number of test-cases.
- Each test-case contains lines, representing time t1 and time t2.
 
#### Constraints
- Input contains only valid timestamps
- Year <= 3000

#### Output Format
Print the absolute difference (t1 - t2) in seconds.

#### Sample Input 0
```
2
Sun 10 May 2015 13:54:36 -0700
Sun 10 May 2015 13:54:36 -0000
Sat 02 May 2015 19:54:36 +0530
Fri 01 May 2015 13:54:36 -0000
```

#### Sample Output 0
```
25200
88200
```
## Challenge 2:
You are given an integer N followed by N email addresses. Your task is to print a list containing
only valid email addresses in lexicographical order.
Valid email addresses must follow these rules:
- It must have the username@domainname.extension format type.
- The username can only contain letters, digits, dashes and underscores.
- The website name can only have letters and digits.
- The maximum length of the extension is 3.
#### Input Format
- The first line of input is the integer N, the number of email addresses.
- N lines follow, each containing a string.
#### Constraints
Each line is a non-empty string.
#### Output Format
Output a list containing the valid email addresses in lexicographical order. If the list is empty, just
output an empty list, [].
#### Sample Input
```
4
clara@example.com
john-doe23@example.com
jane_35@example.com
joe%bloggs@example.com
```
#### Sample Output
```['clara@example.com', 'jane_35@example.com', 'john-doe23@example.com']```


## Instructions
Solution of each challenge could be find from `main.py` file of each package named according to challenges, 
for example solution for challenge number 1 inside the package `first_challenge` -> `main.py` file.

#### Test environment
- Create and activate new virtual environments
- Run `pip install -r requirements.txt`
- Check  whether tests are working or not by `python -m unittest`