import unittest
import random
import pytz

from datetime import datetime, timedelta


from first_challenge.main import calculate_timedelta


class TestFirstChallenge(unittest.TestCase):
    fixtures = {
        'test_1':
            {
                'input': [
                    'Sun 10 May 2015 13:54:36 -0700',
                    'Sun 10 May 2015 13:54:36 -0000',
                    'Sat 02 May 2015 19:54:36 +0530',
                    'Fri 01 May 2015 13:54:36 -0000',
                ],
                'output': ['25200', '88200']
            }
    }

    @staticmethod
    def random_time_stamp_generator(count=100):
        """
        Generate list of random datetime objects and store time difference of
        each pair, then change timezone of each object randomly and save them
        to the input list
        :param count: how many count of time_stamps needed.
        :return: dictionary with a random input and expected_output list
        """
        start_period = datetime(2013, 1, 1, tzinfo=pytz.timezone('utc'))
        end_period = datetime.now(pytz.timezone('utc'))
        output_list = list()
        input_list = list()
        for i in range(count):
            step = timedelta(days=random.randint(1, 5), seconds=random.randint(0, 3600))
            start_time = start_period + random.randrange((end_period - start_period) // step + 1) * step
            end_time = start_period + random.randrange((end_period - start_period) // step + 1) * step
            start_random_tz = start_time.astimezone(pytz.timezone(random.choice(pytz.all_timezones)))
            end_random_tz = end_time.astimezone(pytz.timezone(random.choice(pytz.all_timezones)))
            input_list.append(start_random_tz.strftime("%a %d %b %Y %H:%M:%S %z"))
            input_list.append(end_random_tz.strftime("%a %d %b %Y %H:%M:%S %z"))
            output_list.append(str(abs(int((start_time - end_time).total_seconds()))))
        return {'input': input_list, 'output': output_list}

    def test_time_delta_calculator(self):
        self.assertEqual(calculate_timedelta(len(self.fixtures['test_1']['input']), self.fixtures['test_1']['input']), self.fixtures['test_1']['output'])

    def test_time_delta_calculator_randomly(self):
        fixtures = self.random_time_stamp_generator(1000)
        self.assertEqual(calculate_timedelta(len(fixtures['input']), fixtures['input']), fixtures['output'])


if __name__ == '__main__':
    unittest.main()
