from datetime import datetime


def delta(start_string, end_string):
    """
    Get two datetime in string format, evaluate them to the python datetime
    object and then return absolute value between them in seconds
    :param start_string: datetime string, i.e: 'Sun 10 May 2015 13:54:36 -0700'
    :param end_string:  datetime string, i.e: 'Sun 10 May 2015 13:54:36 -0000'
    :return: difference total_seconds in string format
    """
    start_time = datetime.strptime(start_string, "%a %d %b %Y %H:%M:%S %z")
    end_time = datetime.strptime(end_string, "%a %d %b %Y %H:%M:%S %z")
    difference = str(abs(int((start_time - end_time).total_seconds())))
    print(difference)
    return difference


def calculate_timedelta(count, stamps):
    """
    Get a list of datetime strings and length of the list from input and return,
    time difference between each two consecutive items of list.
    :param count: length of the times_list
    :param stamps: list of datetime string in similar formats, i.e:
    'Sun 10 May 2015 13:54:36 -0700'
    :return: a list of time difference of each pair of datetime in given list
    """
    return [delta(stamps[i * 2], stamps[(i * 2) + 1]) for i in range(count // 2)]
