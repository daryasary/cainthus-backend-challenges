import re


def validate_email(count, emails):
    """
    Get a list of inputs string and check whether they are valid email or not
    and then return sorted list of valid emails,
    :param count: length of the input string
    :param emails: list of input string
    :return: ordered list if valid emails
    """
    pattern = re.compile("^[a-zA-Z0-9_+-]+@[a-zA-Z0-9]+\.[a-zA-Z]{1,3}$")
    return sorted(filter(pattern.match, emails))
