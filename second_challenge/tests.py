import unittest

from second_challenge.main import validate_email


class TestSecondChallenge(unittest.TestCase):
    fixtures = {
        'input': [
            'john-doe23@example.com',
            'jane_35@example.com',
            'clara@example.com',
            'joe%bloggs@example.com',
            "Ho3ein@mail.com",
            "john-doe@domain.com3",
            "john-doe@domain4.com",
            "john-doe@dom$ain.com",
            "john-doe@dom_ain.com",
        ],
        'output': [
            'Ho3ein@mail.com',
            'clara@example.com',
            'jane_35@example.com',
            'john-doe23@example.com',
            'john-doe@domain4.com',
        ]
    }

    def test_email_validator(self):
        self.assertEqual(validate_email(len(self.fixtures['input']), self.fixtures['input']), self.fixtures['output'])


if __name__ == '__main__':
    unittest.main()
